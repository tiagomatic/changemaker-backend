# Use the official maven/Java 11 image to create a build artifact.
# https://hub.docker.com/_/maven
FROM maven:3.6.2-jdk-11-slim as builder

# Copy local code to the container image.
WORKDIR /app
COPY pom.xml ./
COPY system.properties ./
COPY src ./src
COPY .mvn ./
COPY mvnw* ./

# Build a release artifact.
RUN mvn package -DskipTests

# https://hub.docker.com/r/adoptopenjdk/openjdk11
# https://docs.docker.com/develop/develop-images/multistage-build/#use-multi-stage-builds
FROM adoptopenjdk/openjdk11:jdk-11.0.4_11-slim

# Copy the jar to the production image from the builder stage.
COPY --from=builder /app/target/changemaker-*.jar /changemaker.jar

# Run the web service on container startup.
CMD ["java","-Djava.security.egd=file:/dev/./urandom","-Dserver.port=${PORT}","-jar","/changemaker.jar"]
