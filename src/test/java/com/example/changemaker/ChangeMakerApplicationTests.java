package com.example.changemaker;

import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.context.junit4.SpringRunner;

import static com.google.common.truth.Truth.assertWithMessage;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class ChangeMakerApplicationTests {
  @LocalServerPort
  private int port;

  @Autowired
  private TestRestTemplate restTemplate;

  @Test
  public void onlyCentsResponseTest() throws Exception {
    assertWithMessage("Incorrect response to request of 99 cents").that(
        this.restTemplate.getForObject("http://localhost:" + port + "/?dollarAmount=0.99",
            Map.class).values()).containsAllOf(0,1,1,2,0,4);
  }

  @Test
  public void singleDollarAndCentsResponseTest() throws Exception {
    assertWithMessage("Incorrect response to request of $1.56").that(
        this.restTemplate.getForObject("http://localhost:" + port + "/?dollarAmount=1.56",
            Map.class).values()).containsAllOf(1,1,0,0,1,1);
  }

  @Test
  public void dollarsAndCentsResponseTest() throws Exception {
    assertWithMessage("Correct to request of 12.85").that(
        this.restTemplate.getForObject("http://localhost:" + port + "/?dollarAmount=12.85",
            Map.class).values()).containsAllOf(12,1,1,1,0,0);
  }

  @Test
  public void simpleResponseTest() throws Exception {
    assertWithMessage("Incorrect response for simple request").that(
        this.restTemplate.getForObject("http://localhost:" + port + "/?dollarAmount=1234",
            Map.class).values()).containsAllOf(1234,0,0,0,0,0);
  }

  @Test
  public void withDollarSignResponseTest() throws Exception {
    assertWithMessage("Incorrect response to request with dollar sign").that(
        this.restTemplate.getForObject("http://localhost:" + port + "/?dollarAmount=$12.34",
            Map.class).values()).containsAllOf(12,0,1,0,1,4);
  }

  @Test
  public void zeroValueRequestTest() throws Exception {
    assertWithMessage("Incorrect response to request with zero dollar amount").that(
        this.restTemplate.getForObject("http://localhost:" + port + "/?dollarAmount=0.00",
            Map.class).values()).containsAllOf(0,0,0,0,0,0);
  }

  @Test
  public void badRequestTest() throws Exception {
    assertWithMessage("Incorrect response for 400 bad request").that(
        this.restTemplate.getForObject("http://localhost:" + port + "/",
            Map.class).values()).containsAllOf(400, "Bad Request",
        "Required String parameter 'dollarAmount' is not present");
  }

  @Test
  public void notFoundTest() throws Exception {
    assertWithMessage("Incorrect response for 404 no found").that(
        this.restTemplate.getForObject("http://localhost:" + port + "/nonexistentpage",
            Map.class).values()).containsAllOf(404, "Not Found");
  }

  @Test
  public void unexpectedErrorTest() throws Exception {
    assertWithMessage("Incorrect response for 500 server error").that(
        this.restTemplate.getForObject("http://localhost:" + port + "/?dollarAmount=23,20",
            Map.class).values()).containsAllOf(
                500, "Internal Server Error", "For input string: \"23,20\"");
  }
}