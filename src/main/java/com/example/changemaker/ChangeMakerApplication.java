package com.example.changemaker;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
public class ChangeMakerApplication {

//  @Value("${TARGET:$0.00}")
//  private String value;

  @RestController
  class ChangeMakerController {
    @GetMapping("/")
    @CrossOrigin(origins = "*")
    Map response(@RequestParam(required = true) String dollarAmount) {
      HashMap<String, String> map = new HashMap<>();
      return makeChange(dollarAmount);
    }

    /**
     * This method takes in a string dollar amount, converts it to an integer, and calculates
     * the denomination amounts to build a hash map.
     *
     * @param dollarAmount
     * @return changeMap
     */

    HashMap<String, Integer> makeChange(String dollarAmount){
      int dollarAmountInt = Math.round(Float.parseFloat(dollarAmount.replace("$","")) * 100);

      LinkedHashMap<String, Integer> denominationMap = new LinkedHashMap<>();
      denominationMap.put("silver-dollar",100);
      denominationMap.put("half-dollar",50);
      denominationMap.put("quarter",25);
      denominationMap.put("dime",10);
      denominationMap.put("nickel",5);
      denominationMap.put("penny",1);

      HashMap<String, Integer> changeMap = new HashMap<>();
      denominationMap.forEach((denomination,value)-> {
        if (denomination.equals("silver-dollar")) {
          changeMap.put(denomination, dollarAmountInt / value);
          changeMap.put("remainder", dollarAmountInt % value);
        } else {
          changeMap.put(denomination, changeMap.get("remainder") / value);
          changeMap.put("remainder", changeMap.get("remainder") % value);
        }

        if (denomination.equals("penny")){
          changeMap.remove("remainder");
        }
      });
      return changeMap;
    }
  }

  public static void main(String[] args) {
		SpringApplication.run(ChangeMakerApplication.class, args);
	}
}